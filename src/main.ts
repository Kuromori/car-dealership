import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true, //solo deja la data requerida del body
     forbidNonWhitelisted: true //valida si los campos enviados en el body existen en el dto
    })
  );

  await app.listen(3000);
}
bootstrap();
