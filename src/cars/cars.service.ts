import { Injectable, NotFoundException } from '@nestjs/common';
import { Car } from './interfaces/car.interface';
import { v4 as uuid } from "uuid";
import { CreateCarDto } from './dto/create-car.dto';
import { UpdateCarDto } from './dto/update-car.dto';

@Injectable()
export class CarsService {
  private allCars: Car[] = [
    {
      id: uuid(),
      brand: 'TOYOTA',
      model: 'COROLLA'
    },
    {
      id: uuid(),
      brand: 'HONDA',
      model: 'CIVIC'
    },
    {
      id: uuid(),
      brand: 'JEEP',
      model: 'CHEROKEE'
    },
  ]

  findAll() {return this.allCars;}
  findCar(id: string){
    const car = this.allCars.find(car => id === car.id )
    if(!car) throw new NotFoundException(`Car ${id} not found`);
    return car;
  }

  insertCard(body: CreateCarDto){ 
    const newCar: Car = {id: uuid(), ...body};
    this.allCars.push(newCar);
    return newCar;
  }

  updateCar(id:string, body: UpdateCarDto){
    let findCar = this.findCar(id);
    if(!findCar) throw new NotFoundException(`Car ${id} not found`);
    findCar = {...findCar, ...body};
    return findCar;
  }

  deleteCar(id: string){
    const findCar = this.findCar(id);
    if(!findCar) throw new NotFoundException(`Car ${id} not found`);
    this.allCars = this.allCars.filter(car => car.id !== id)
    return;
  }

  fillCarsWithSeedData(cars: Car[]){
    this.allCars = cars;
  }

}
