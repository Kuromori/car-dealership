import { IsOptional, IsString, IsUUID } from "class-validator";

export class UpdateCarDto{

  @IsString()
  @IsUUID()
  @IsOptional()
  readonly id?: string;

  @IsString({message: 'Brand must be string'})
  @IsOptional()
  readonly brand?: string;

  @IsString({message: 'Model must be string'})
  @IsOptional()
  readonly model?: string;
}