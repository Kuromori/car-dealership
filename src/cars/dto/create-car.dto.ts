import { IsString } from "class-validator";

export class CreateCarDto{

  @IsString({message: 'Brand must be string'})
  readonly brand: string;

  @IsString({message: 'Model must be string'})
  readonly model: string;
}