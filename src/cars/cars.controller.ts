import { Body, Controller, Delete, Get, NotFoundException, Param, ParseIntPipe, ParseUUIDPipe, Patch, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { CarsService } from './cars.service';
import { CreateCarDto } from './dto/create-car.dto';
import { UpdateCarDto } from './dto/update-car.dto';

@Controller('cars')
//@UsePipes(ValidationPipe)
export class CarsController {

  constructor(
    private readonly carsService: CarsService
  ) { }

  @Get()
  getAllCards(): any[] {
    return this.carsService.findAll();
  }

  @Get(':id')
  getCardById(@Param('id', ParseUUIDPipe) id: string) {
    const car = this.carsService.findCar(id);
    return car;
  }

  @Post()
  createCar(@Body() createCarDto: CreateCarDto){
    this.carsService.insertCard(createCarDto);
  }

  @Patch(':id')
  updateCar(@Param('id') id: string,  @Body() body: UpdateCarDto){
    //return body;
    this.carsService.updateCar(id, body);
  }

  @Delete(':id')
  deleteCar(id: number,  @Body() body: any){
    return body;
    this.carsService.insertCard(body);
  }

}
