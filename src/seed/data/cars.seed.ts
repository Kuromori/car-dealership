import { v4 as uuid } from "uuid";
import { Car } from "src/cars/interfaces/car.interface";

export const CARS_SEED: Car[] = [
  {
    id: uuid(),
    brand: 'TOYOTA',
    model: 'COROLLA'
  },
  {
    id: uuid(),
    brand: 'HONDA',
    model: 'CIVIC'
  },
  {
    id: uuid(),
    brand: 'SUZUKI',
    model: 'SWIFT'
  },
];