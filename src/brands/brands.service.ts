import { Injectable, NotFoundException } from '@nestjs/common';
import { v4 as uuid } from "uuid";


import { CreateBrandDto } from './dto/create-brand.dto';
import { UpdateBrandDto } from './dto/update-brand.dto';
import { Brand } from './entities/brand.entity';

@Injectable()
export class BrandsService {

  private brands: Brand[] = [
    {
      id: uuid(),
      name: 'TOYOTA',
      createdAt: new Date().getTime()
    }
  ]

  create(createBrandDto: CreateBrandDto) {
    const brand: Brand = {
      id: uuid(),
      name: createBrandDto.name,
      createdAt: new Date().getTime()
    }
    this.brands.push(brand);
    return brand;
  }

  findAll() {
    return this.brands;
  }

  findOne(id: string) {
    const brand = this.brands.find(brand => brand.id === id);
    if(!brand) throw new NotFoundException(`Brand with id ${id} not found`);
    return brand;
  }

  update(id: string, updateBrandDto: UpdateBrandDto) {
    const brandDB = this.brands.find(brand => brand.id === id);
    if(!brandDB) throw new NotFoundException(`Brand with id ${id} not found`);
    this.brands = this.brands.map(brand => {
      if(brand.id === id){
        brand.updatedAt = new Date().getTime(),
        brand = {
          ...brand,
          ...updateBrandDto
        }
        return brand;
      }
      return brand;
    })
  }

  remove(id: string) {
    const brandDB = this.brands.find(brand => brand.id === id);
    if(!brandDB) throw new NotFoundException(`Brand with id ${id} not found`);
    this.brands.filter(brand => brand.id !== id);
    return;
  }

  fillBrandsWithSeedData(brands: Brand[]){
    this.brands = brands;
  }

}
